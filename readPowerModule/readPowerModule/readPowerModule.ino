/*
 this method reads current and voltage from pixhawk power module
*/

/*
1S - 3.7V
2S - 7.4V
3S - 11.1V
4S - 14.8V
5S - 18.5V
6S - 22.2V
*/

#define BATT_VOLT_PIN 0; // voltage read pin
#define BATT_CURR_PIN 1; // current read pin


int raw_voltage; //This will store our raw ADC data
int raw_current;
float mesured_voltage; //This will store the converted data
float mesured_current;

int BATT_N_CELLS = 3;   // number of cells in battery

// constants
// VOLTAGE
// 49.44; //45 Amp board
// 12.99; //90 Amp board
// 12.99; //180 Amp board 

// CURRENTS
// 14.9; //45 Amp board
// 7.4; //90 Amp board
// 3.7; //180 Amp board



float BATT_VOLT_SCALING = 49.44; // - scaling coefficient for voltage sensor
float BATT_CURR_SCALING = 14.9; //scaling coefficient for current sensor

// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(57600);
}

// the loop function runs over and over again until power down or reset
void loop() {
		
	//Measurement
	raw_voltage = analogRead(BATT_VOLT_PIN);
	raw_current = analogRead(BATT_CURR_PIN);

	//Conversion
	mesured_voltage = raw_voltage / BATT_VOLT_SCALING;
	mesured_current = raw_current / BATT_CURR_SCALING;


	//Display
	Serial.print(mesured_voltage);
	Serial.print("   Volts");
	Serial.print(mesured_current);
	Serial.print("   Amps");
	delay(200);

}
